package com.example.karan.school;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    EditText getInput;
    Button toEuro;
    Button toUSD;
    TextView omgerekend;
    Double input = Double.parseDouble(getInput.getText().toString());

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getInput = (EditText) findViewById(R.id.inputText);
        toEuro = (Button) findViewById(R.id.naarEuroButton);
        toUSD = (Button) findViewById(R.id.naarUSDButton);
        omgerekend = (TextView) findViewById(R.id.omgerekend);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void closeKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void usdClick(View v) {
        Double omrekening = input * 1.121;
        String antwoord = String.valueOf(String.format("%.2f", omrekening));
        omgerekend.setText("$ " + antwoord);
        closeKeyboard();
    }

    public void euClick(View v) {
        Double omrekening = input * 0.892;
        String antwoord = String.valueOf(String.format("%.2f",omrekening));
        omgerekend.setText("€ " + antwoord);
        closeKeyboard();
    }
}
